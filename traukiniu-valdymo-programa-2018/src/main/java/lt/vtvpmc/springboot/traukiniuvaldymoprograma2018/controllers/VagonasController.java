//package lt.vtvpmc.springboot.traukiniuvaldymoprograma2018.controllers;
//
//
//import lt.vtvpmc.springboot.traukiniuvaldymoprograma2018.models.Vagonas;
//import lt.vtvpmc.springboot.traukiniuvaldymoprograma2018.services.VagonasService;
//import org.springframework.http.HttpStatus;
//import org.springframework.web.bind.annotation.*;
//
//import javax.validation.Valid;
//import java.util.List;
//
//@CrossOrigin
//@RestController
//@RequestMapping(value = "/api/vagonai")
//public class VagonasController {
//
//
//        private VagonasService vagonasService;
//
//        public VagonasController(VagonasService vagonasService) {
//            this.vagonasService = vagonasService;
//        }
//
//        @RequestMapping(value = "/{get}", method = RequestMethod.GET)
//        public List<Vagonas> getVagonai(){
//            return this.vagonasService.getVagonai();
//        }
//
//        @RequestMapping(value = "/{post}", method = RequestMethod.POST)
//        @ResponseStatus(HttpStatus.CREATED)
//        public void createVagonai(@RequestBody @Valid final Vagonas vagonas){
//            this.vagonasService.createVagonai(vagonas);
//        }
//}
