package lt.vtvpmc.springboot.traukiniuvaldymoprograma2018.models;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Entity
public class Traukinys {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private  Long id;
    private Long numeris;
    @Temporal(TemporalType.DATE)
    private Date pagaminimoData;
    private String pagaminusiImone;
    private  String miestas;
    @Transient
    private BigDecimal bendraKaina;

    @OneToMany(mappedBy = "traukinys", cascade = CascadeType.ALL)
    private List<Vagonas> vagonas;



    public Traukinys() {
    }

    public void setNumeris(Long numeris) {
        this.numeris = numeris;
    }

    public Long getNumeris() {
        return numeris;
    }



    public void setBendraKaina(BigDecimal bendraKaina) {
        this.bendraKaina = bendraKaina;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getPagaminimoData() {
        return pagaminimoData;
    }

    public void setPagaminimoData(Date pagaminimoData) {
        this.pagaminimoData = pagaminimoData;
    }

    public String getPagaminusiImone() {
        return pagaminusiImone;
    }

    public void setPagaminusiImone(String pagaminusiImone) {
        this.pagaminusiImone = pagaminusiImone;
    }

    public String getMiestas() {
        return miestas;
    }

    public void setMiestas(String miestas) {
        this.miestas = miestas;
    }

    public List<Vagonas> getVagonas() {
        return vagonas;
    }

    public void setVagonas(List<Vagonas> vagonas) {
        this.vagonas = vagonas;
    }

    public BigDecimal getBendraKaina(){
        BigDecimal bendraKaina= new BigDecimal("0");
        for (Vagonas v: this.getVagonas()) {
            bendraKaina = bendraKaina.add(v.getKaina());
        }
        return bendraKaina;
    }
}
