package lt.vtvpmc.springboot.traukiniuvaldymoprograma2018.services;

import lt.vtvpmc.springboot.traukiniuvaldymoprograma2018.models.Traukinys;
import lt.vtvpmc.springboot.traukiniuvaldymoprograma2018.models.Vagonas;
import lt.vtvpmc.springboot.traukiniuvaldymoprograma2018.repositories.VagonasRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class VagonasService {

    private VagonasRepository vagonasRepository;

    public VagonasService(VagonasRepository vagonasRepository) {
        this.vagonasRepository = vagonasRepository;
    }

    @Transactional(readOnly = true)
    public List<Vagonas> getVagonai(){
        return vagonasRepository.findAll();
    }

    @Transactional
    public void createVagonai(Vagonas vagonas) {
        vagonasRepository.save(vagonas);
    }

    @Transactional
    public void deleteVagonai(Long id) {
        vagonasRepository.deleteById(id);
    }

    @Transactional
    public void updateVagonai(Long id, Vagonas vagonas) {
        vagonasRepository.save(vagonas);
    }
}