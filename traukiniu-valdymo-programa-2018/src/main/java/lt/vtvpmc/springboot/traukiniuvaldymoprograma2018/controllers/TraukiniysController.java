package lt.vtvpmc.springboot.traukiniuvaldymoprograma2018.controllers;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lt.vtvpmc.springboot.traukiniuvaldymoprograma2018.CreateVagonas;
import lt.vtvpmc.springboot.traukiniuvaldymoprograma2018.models.Traukinys;
import lt.vtvpmc.springboot.traukiniuvaldymoprograma2018.services.TraukinysService;
import lt.vtvpmc.springboot.traukiniuvaldymoprograma2018.services.VagonasService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@Api(value = "traukinys")
@RestController
@RequestMapping(value = "/api/traukiniai")
public class TraukiniysController {

    private TraukinysService traukinysService;
    private VagonasService vagonasService;

    public TraukiniysController(TraukinysService traukinysService, VagonasService vagonasService) {
        this.traukinysService = traukinysService;
        this.vagonasService = vagonasService;
    }

    @GetMapping
    @ApiOperation(value = "Get traukinys", notes = "Gražina traukinių sąrašą")
    public List<Traukinys> getTraukiniai() {
        return this.traukinysService.getTruakiniai();
    }


    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public void createTraukinys(@RequestBody @ApiParam Traukinys traukinys) {
        this.traukinysService.createTraukinys(traukinys);
    }

    @DeleteMapping(path = "/{id}")
    public void deleteTraukinys(@PathVariable @ApiParam Long id) {
        traukinysService.deleteTraukinys(id);
    }

    @PostMapping(path = "/{id}")
    public void pridetiVagona(@PathVariable @ApiParam Long id, @RequestBody @ApiParam CreateVagonas createVagonas) {
        traukinysService.pridetiVagona(id, createVagonas);
    }

    @DeleteMapping(path = "/{traukinysId}/{vagonasId}")
    public void deleteVagonas(@PathVariable @ApiParam Long traukinysId, @PathVariable @ApiParam Long vagonasId) {
        traukinysService.deleteVagonas(traukinysId,vagonasId);
    }
}
