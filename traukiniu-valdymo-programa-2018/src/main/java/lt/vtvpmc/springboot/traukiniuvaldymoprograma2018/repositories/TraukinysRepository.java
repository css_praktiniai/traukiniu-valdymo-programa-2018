package lt.vtvpmc.springboot.traukiniuvaldymoprograma2018.repositories;

import lt.vtvpmc.springboot.traukiniuvaldymoprograma2018.models.Traukinys;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TraukinysRepository extends JpaRepository<Traukinys, Long>{
}
