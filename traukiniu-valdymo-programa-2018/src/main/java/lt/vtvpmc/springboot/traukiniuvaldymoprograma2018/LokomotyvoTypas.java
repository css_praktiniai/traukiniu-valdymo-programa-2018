package lt.vtvpmc.springboot.traukiniuvaldymoprograma2018;

public enum LokomotyvoTypas
{
    STUMIANTIS, TRAUKIANTIS
}
