package lt.vtvpmc.springboot.traukiniuvaldymoprograma2018;

import lt.vtvpmc.springboot.traukiniuvaldymoprograma2018.models.Vagonas;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Date;

public class CreateVagonas extends Vagonas {
    @NotNull
    private Integer numeris;

    @Temporal(TemporalType.DATE)
    private Date pagaminimoData;

    @NotNull
    private String pagaminusiImone;

    @NotNull
    private  String miestas;

    @NotNull
    private String gamintojas;
    @NotNull
    private Integer kiekis;
    @NotNull
    private BigDecimal kaina;
    @NotNull
    private double turis; //kubiniai metrai
    private Integer klase;

    private double maksimaliKelGalia; //tonos

    private VagonoTypas vagonoTypas;

    private LokomotyvoTypas lokomotyvoTypas;

    public VagonoTypas getVagonoTypas() {
        return vagonoTypas;
    }

    public void setVagonoTypas(VagonoTypas vagonoTypas) {
        this.vagonoTypas = vagonoTypas;
    }

    public Integer getNumeris() {
        return numeris;
    }

    public void setNumeris(Integer numeris) {
        this.numeris = numeris;
    }

    public Date getPagaminimoData() {
        return pagaminimoData;
    }

    public void setPagaminimoData(Date pagaminimoData) {
        this.pagaminimoData = pagaminimoData;
    }

    public String getPagaminusiImone() {
        return pagaminusiImone;
    }

    public void setPagaminusiImone(String pagaminusiImone) {
        this.pagaminusiImone = pagaminusiImone;
    }

    public String getMiestas() {
        return miestas;
    }

    public void setMiestas(String miestas) {
        this.miestas = miestas;
    }

    public String getGamintojas() {
        return gamintojas;
    }

    public void setGamintojas(String gamintojas) {
        this.gamintojas = gamintojas;
    }

    public Integer getKiekis() {
        return kiekis;
    }

    public void setKiekis(Integer kiekis) {
        this.kiekis = kiekis;
    }

    public BigDecimal getKaina() {
        return kaina;
    }

    public void setKaina(BigDecimal kaina) {
        this.kaina = kaina;
    }

    public double getTuris() {
        return turis;
    }

    public void setTuris(double turis) {
        this.turis = turis;
    }

    public Integer getKlase() {
        return klase;
    }

    public void setKlase(Integer klase) {
        this.klase = klase;
    }

    public double getMaksimaliKelGalia() {
        return maksimaliKelGalia;
    }

    public void setMaksimaliKelGalia(double maksimaliKelGalia) {
        this.maksimaliKelGalia = maksimaliKelGalia;
    }

    public LokomotyvoTypas getLokomotyvoTypas() {
        return lokomotyvoTypas;
    }

    public void setLokomotyvoTypas(LokomotyvoTypas lokomotyvoTypas) {
        this.lokomotyvoTypas = lokomotyvoTypas;
    }
}
