package lt.vtvpmc.springboot.traukiniuvaldymoprograma2018.models;

import javax.persistence.Entity;


@Entity
public class KeleivinisVagonas extends Vagonas {

    private Integer klase;

    public Integer getKlase() {
        return klase;
    }

    public void setKlase(Integer klase) {
        this.klase = klase;
    }
}
