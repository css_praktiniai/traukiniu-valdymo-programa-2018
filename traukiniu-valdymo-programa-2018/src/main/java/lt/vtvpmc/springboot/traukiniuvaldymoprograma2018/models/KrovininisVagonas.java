package lt.vtvpmc.springboot.traukiniuvaldymoprograma2018.models;

import javax.persistence.Entity;

@Entity
public class KrovininisVagonas extends Vagonas {

    private double maksimaliKelGalia; //tonos

    public double getMaksimaliKelGalia() {
        return maksimaliKelGalia;
    }

    public void setMaksimaliKelGalia(double maksimaliKelGalia) {
        this.maksimaliKelGalia = maksimaliKelGalia;
    }
}
