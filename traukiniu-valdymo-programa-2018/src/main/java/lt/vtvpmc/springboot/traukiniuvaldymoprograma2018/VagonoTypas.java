package lt.vtvpmc.springboot.traukiniuvaldymoprograma2018;

public enum  VagonoTypas
{
    KELEIVINIS, KROVININIS, LOKOMOTYVAS
}
