package lt.vtvpmc.springboot.traukiniuvaldymoprograma2018.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Entity
public abstract class Vagonas {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    //@NotNull
    private String gamintojas;
    //@NotNull
    private Integer kiekis;
    //@NotNull
    private BigDecimal kaina;
    //@NotNull
    private double turis; //kubiniai metrai

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "TRAUKINIAI_ID")
    @JsonIgnore
    private Traukinys traukinys;

    public Vagonas() {
    }

    public Traukinys getTraukinys() {
        return traukinys;
    }

    public void setTraukinys(Traukinys traukinys) {
        this.traukinys = traukinys;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getGamintojas() {
        return gamintojas;
    }

    public void setGamintojas(String gamintojas) {
        this.gamintojas = gamintojas;
    }

    public Integer getKiekis() {
        return kiekis;
    }

    public void setKiekis(Integer kiekis) {
        this.kiekis = kiekis;
    }

    public BigDecimal getKaina() {
        return kaina;
    }

    public void setKaina(BigDecimal kaina) {
        this.kaina = kaina;
    }

    public double getTuris() {
        return turis;
    }

    public void setTuris(double turis) {
        this.turis = turis;
    }
}


