package lt.vtvpmc.springboot.traukiniuvaldymoprograma2018.models;

import lt.vtvpmc.springboot.traukiniuvaldymoprograma2018.LokomotyvoTypas;

import javax.persistence.Entity;

@Entity
public class Lokomotyvas extends Vagonas {

    private LokomotyvoTypas lokomotyvoTypas;

    public Lokomotyvas(LokomotyvoTypas lokomotyvoTypas)
    {
        this.lokomotyvoTypas = lokomotyvoTypas;
    }

    public Lokomotyvas() {
    }

    public LokomotyvoTypas getLokomotyvoTypas()
    {
        return lokomotyvoTypas;
    }

    public void setLokomotyvoTypas(LokomotyvoTypas lokomotyvoTypas) {
        this.lokomotyvoTypas = lokomotyvoTypas;
    }
}
