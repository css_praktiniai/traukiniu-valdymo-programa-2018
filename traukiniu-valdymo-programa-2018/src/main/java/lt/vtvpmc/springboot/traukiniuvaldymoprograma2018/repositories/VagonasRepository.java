package lt.vtvpmc.springboot.traukiniuvaldymoprograma2018.repositories;

import lt.vtvpmc.springboot.traukiniuvaldymoprograma2018.models.Vagonas;
import org.springframework.data.jpa.repository.JpaRepository;

public interface VagonasRepository extends JpaRepository<Vagonas, Long>{
}
