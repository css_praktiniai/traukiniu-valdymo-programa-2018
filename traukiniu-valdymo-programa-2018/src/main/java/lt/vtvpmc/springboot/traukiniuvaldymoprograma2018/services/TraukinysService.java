package lt.vtvpmc.springboot.traukiniuvaldymoprograma2018.services;

import lt.vtvpmc.springboot.traukiniuvaldymoprograma2018.CreateVagonas;
import lt.vtvpmc.springboot.traukiniuvaldymoprograma2018.LokomotyvoTypas;
import lt.vtvpmc.springboot.traukiniuvaldymoprograma2018.VagonoTypas;
import lt.vtvpmc.springboot.traukiniuvaldymoprograma2018.models.*;
import lt.vtvpmc.springboot.traukiniuvaldymoprograma2018.repositories.TraukinysRepository;
import lt.vtvpmc.springboot.traukiniuvaldymoprograma2018.repositories.VagonasRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class TraukinysService {

    @Autowired
    private TraukinysRepository traukinysRepository;
    @Autowired
    private VagonasRepository vagonasRepository;


    public TraukinysService() {
    }

    public TraukinysService(TraukinysRepository traukinysRepository, VagonasRepository vagonasRepository) {
        this.traukinysRepository = traukinysRepository;
        this.vagonasRepository = vagonasRepository;

    }

    @Transactional
    public List<Traukinys> getTruakiniai() {
        return traukinysRepository.findAll();
    }

    @Transactional
    public void createTraukinys(Traukinys traukinys) {
        traukinysRepository.save(traukinys);
    }

    @Transactional
    public void deleteTraukinys(Long id) {
        traukinysRepository.deleteById(id);
    }

    @Transactional
    public void updateTraukinys(Long id, Traukinys traukinys) {
        traukinysRepository.save(traukinys);
    }


    @Transactional
    public void pridetiVagona(Long id, CreateVagonas createVagonas) {
        switch (createVagonas.getVagonoTypas()) {
            case KELEIVINIS:
                KeleivinisVagonas keleivinisVagonas = new KeleivinisVagonas();
                keleivinisVagonas.setGamintojas(createVagonas.getGamintojas());
                keleivinisVagonas.setKiekis(createVagonas.getKiekis());
                keleivinisVagonas.setKaina(createVagonas.getKaina());
                keleivinisVagonas.setTuris(createVagonas.getTuris());
                keleivinisVagonas.setKlase(createVagonas.getKlase());
                keleivinisVagonas.setTraukinys(traukinysRepository.findById(id).get());
                vagonasRepository.save(keleivinisVagonas);
                break;

            case KROVININIS:
                KrovininisVagonas krovininisVagonas = new KrovininisVagonas();
                krovininisVagonas.setGamintojas(createVagonas.getGamintojas());
                krovininisVagonas.setKiekis(createVagonas.getKiekis());
                krovininisVagonas.setKaina(createVagonas.getKaina());
                krovininisVagonas.setTuris(createVagonas.getTuris());
                krovininisVagonas.setMaksimaliKelGalia(createVagonas.getMaksimaliKelGalia());
                krovininisVagonas.setTraukinys(traukinysRepository.findById(id).get());
                vagonasRepository.save(krovininisVagonas);
                break;

            case LOKOMOTYVAS:
                Lokomotyvas lokomotyvas = new Lokomotyvas();
                lokomotyvas.setGamintojas(createVagonas.getGamintojas());
                lokomotyvas.setKiekis(createVagonas.getKiekis());
                lokomotyvas.setKaina(createVagonas.getKaina());
                lokomotyvas.setTuris(createVagonas.getTuris());
                lokomotyvas.setLokomotyvoTypas(createVagonas.getLokomotyvoTypas());
                lokomotyvas.setTraukinys(traukinysRepository.findById(id).get());
                vagonasRepository.save(lokomotyvas);
                break;

        }
    }

    @Transactional
    public void deleteVagonas(Long traukinysId, Long vagonasId) {
        vagonasRepository.delete(vagonasRepository.findById(vagonasId).get());
    }
}
